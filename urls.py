from config.http_methods import HttpMethod
from views.views import index, get_config, update_config


URLS = [
    ("/", HttpMethod.GET, index),
    ("/config", HttpMethod.GET, get_config),
    ("/config", HttpMethod.POST, update_config)
]