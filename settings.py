DATABASE = {
    "host": "localhost",
    "port": 27017,
    "db": "pyconfmdb"
}

VALIDATORS = [
    'validators.request_validator.RequestValidator'
]