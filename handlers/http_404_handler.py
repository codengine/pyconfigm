import json
from handlers.http_error_handler import HttpErrorResponse


class Http404Response(HttpErrorResponse):
    def __init__(self, content=""):
        if type(content) is dict:
            try:
                content = json.dumps(content)
            except Exception as exp:
                content = json.dumps({'message': content})
        else:
            content = json.dumps({'message': content})
        super(Http404Response, self).__init__(text=content, status=404, content_type='application/json')