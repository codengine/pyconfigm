from handlers.http_response import HttpResponse


class HttpErrorResponse(HttpResponse):
    pass