from aiohttp import web


class HttpResponse(web.Response):
    def __init__(self, body=None, status=200,
                 reason=None, text=None, headers=None, content_type=None,
                 charset=None):
        super(HttpResponse, self).__init__(body=body, status=status, reason=reason, text=text,
                                           headers=headers, content_type=content_type,
                                           charset=charset)