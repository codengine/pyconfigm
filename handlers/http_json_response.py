import json
from handlers.http_response import HttpResponse


class HttpJsonResponse(HttpResponse):
    def __init__(self, content={}, status=200):
        if type(content) is dict:
            try:
                content = json.dumps(content)
                super(HttpJsonResponse, self).__init__(text=content, status=status,
                                                       content_type='application/json')
            except Exception as exp:
                super(HttpJsonResponse, self).__init__(text="Internal Server Error", status=500,
                                                       content_type='application/json')
        else:
            super(HttpJsonResponse, self).__init__(text="Internal Server Error", status=500,
                                                   content_type='application/json')
