from decorators.enable_validators import enable_validators
from handlers.http_404_handler import Http404Response
from handlers.http_500_handler import Http500Response
from handlers.http_json_response import HttpJsonResponse


async def index(request, DB):
    return HttpJsonResponse(content={'status': 'OK'}, status=200)
    

@enable_validators(validators=['validators.get_request_validator.GETRequestValidator'])
async def get_config(request, DB):
    query_params = request.query
    json = {key: value for key, value in query_params.items()}
    tenant = json['tenant']
    integration_type = json['integration_type']
    config_collection = await DB.get_config(tenant, integration_type)
    if config_collection:
        return HttpJsonResponse(content=config_collection, status=200)
    else:
        return Http404Response(content="Not Found")
    

@enable_validators()
async def update_config(request, DB):
    json = await request.json()
    tenant = json['tenant']
    integration_type = json['integration_type']
    configuration = json['configuration']
    config_collection, created = await DB.update_config(tenant=tenant, integration_type=integration_type, configuration=configuration)
    if config_collection:
        status = 200
        if created:
            status = 201
        return HttpJsonResponse(content=config_collection, status=status)
    else:
        return Http500Response(content="Internal Server Error")