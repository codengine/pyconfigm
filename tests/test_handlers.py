from handlers.http_404_handler import Http404Response
from handlers.http_500_handler import Http500Response
from handlers.http_bad_request import HttpBadRequest
from handlers.http_error_handler import HttpErrorResponse
from handlers.http_json_response import HttpJsonResponse
from handlers.http_response import HttpResponse
from tests.test_base import TestBase


class TestHandlers(TestBase):
    def test_loaders(self):
        assert(issubclass(HttpErrorResponse, HttpResponse) == True)
        assert(issubclass(Http404Response, HttpErrorResponse) == True)
        assert(Http404Response(content="404 Not Found").status == 404)

        assert (issubclass(Http500Response, HttpErrorResponse) == True)
        assert (Http500Response(content="Internal Server Error").status == 500)

        assert (issubclass(HttpBadRequest, HttpErrorResponse) == True)
        assert (HttpBadRequest(content="Bad Request").status == 400)

        assert(issubclass(HttpJsonResponse, HttpResponse) == True)
        assert(HttpJsonResponse(content={'status': 'ok'}).status == 200)
        assert (HttpJsonResponse(content={'status': 'ok'}, status=201).status == 201)
        assert (HttpJsonResponse(content='').status == 500)
        assert (HttpJsonResponse(content={'message': ['a', 'b', '']}).status == 200)
        assert(HttpJsonResponse(content={'message': ['a', 'b', 'c']}).status == 200)

