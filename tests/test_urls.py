from config.http_methods import HttpMethod
from tests.test_base import TestBase
from urls import URLS


class TestUrls(TestBase):
    def test_urls(self):
        all_with_three_params = [len(url) == 3 for url in URLS]
        assert(all(all_with_three_params) == True)
        all_from_http_method_enum = [hasattr(url[1], 'value') for url in URLS]
        assert(all(all_from_http_method_enum) == True)
        all_views_are_callable = [callable(url[2]) for url in URLS]
        assert (all(all_views_are_callable) == True)
        all_methods = [url[1].value for url in URLS]
        all_methods = list(set(all_methods))
        assert(len(list(set(all_methods) & set(HttpMethod.get_supported_methods()))) == len(all_methods))