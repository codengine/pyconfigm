from tests.test_base import TestBase
from urls import URLS
import inspect


class TestViews(TestBase):
    def test_loaders(self):
        all_views = [url[2] for url in URLS]
        all_coroutines = [inspect.iscoroutinefunction(v) for v in all_views]
        assert(all(all_coroutines) == True)
        all_views_with_two_parameters = [v.__code__.co_argcount == 2 for v in all_views]
        assert(all(all_views_with_two_parameters) == True)