import inspect

from database.mongo import Mongo
from decorators.db_wrapper import wrap_db
from aiohttp import web

from decorators.enable_routers import enable_routers
from decorators.enable_validators import enable_validators
from tests.test_base import TestBase


class TestDecorators(TestBase):
    def test_wrap_db_decorator(self):
        async def f(param):
            pass
        wrapped = wrap_db(f, DB='DB')
        assert(inspect.iscoroutinefunction(wrapped) == True)
        assert((wrapped.__code__.co_argcount == 1) == True)

    def test_enable_routers(self):
        DB = Mongo()

        class AsyncTestServer(object):
            def __init__(self):
                self.app = web.Application()

            def start(self, host=None, port=None):
                try:
                    host = host or '127.0.0.1'
                    port = port or 8080
                    web.run_app(self.app, host=host, port=port)
                except Exception as exp:
                    print(str(exp))

        server = AsyncTestServer()
        args_count = server.start.__code__.co_argcount
        router_enabled = enable_routers(DB)(server.start)
        assert((args_count - 1 == len(list(inspect.signature(router_enabled).parameters.keys()))) == True)

    def test_enable_validators(self):
        async def f(param):
            pass

        async def f2(param):
            pass

        wrapped = enable_validators(validators=['validators.get_request_validator.GETRequestValidator'])
        wrapped = wrapped(f)
        assert((wrapped.__code__.co_argcount == 2) == True)
        assert ((wrapped.__code__.co_argcount == 1) == False)
