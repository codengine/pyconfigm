from loaders.loaders import load_module
from settings import VALIDATORS
from tests.test_base import TestBase
from validators.validator import AbstractValidator


class TestValidators(TestBase):
    def test_loaders(self):
        validators = VALIDATORS
        all_validators = [load_module(v) for v in validators]
        all_child_from_abstract = [issubclass(v, AbstractValidator) for v in all_validators]
        assert(all(all_child_from_abstract) == True)