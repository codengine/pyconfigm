from enum import Enum


class HttpMethod(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"

    @classmethod
    def get_supported_methods(cls):
        return [
            HttpMethod.GET.value,
            HttpMethod.POST.value,
            HttpMethod.PUT.value,
            HttpMethod.DELETE.value,
        ]