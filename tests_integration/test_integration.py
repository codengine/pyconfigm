import pytest
from aiohttp import web
from database.mongo import Mongo
from decorators.db_wrapper import wrap_db
from handlers.http_json_response import HttpJsonResponse
from views.views import index, get_config, update_config

pytest_plugins = 'aiohttp.pytest_plugin'


class TestIntegration:

    @pytest.fixture
    def create_DB(self):
        DB = Mongo()
        return DB

    async def index(self, request, DB):
        return HttpJsonResponse(content={'status': 'OK'}, status=200)

    @pytest.fixture
    def create_app_index(self, loop):
        app = web.Application(loop=loop)
        DB = self.create_DB()
        wrapped_index = wrap_db(self.index, DB)
        wrapped_get = wrap_db(get_config, DB)
        wrapped_post = wrap_db(update_config, DB)
        app.router.add_route('GET', '/', wrapped_index)
        app.router.add_route('GET', '/config', wrapped_get)
        app.router.add_route('POST', '/config', wrapped_post)
        return app

    async def test_index(self, test_client):
        client = await test_client(self.create_app_index)
        resp = await client.get('/')
        assert(resp.status == 200)
        json_response = await resp.json()
        assert({"status": "OK"} == json_response)

    async def test_api_update(self, test_client):
        client = await test_client(self.create_app_index)
        resp = await client.get('/config?tenant=abc&integration_type=flight-information-system')
        assert(resp.status == 404)
        post_body = {
            "tenant": "acme",
            "integration_type": "flight-information-system",
            "configuration":
            {
                "username": "acme_user",
                "password": "acme12345",
                "wsdl_urls":
                {
                    "session_url": "https://session.manager.svc",
                    "booking_url": "https://booking.manager.svc",
                    "new_url": "asd1",
                    "new_url_2": "Hello"
                }
            }
        }
        resp = await client.post('/config', json=post_body)
        assert(resp.status in [200, 201])
        json_response = await resp.json()
        assert(json_response == post_body)
        resp = await client.post('/config', data=post_body)
        assert (resp.status == 500)

    async def test_api_get(self, test_client):
        client = await test_client(self.create_app_index)
        resp = await client.get('/config')
        assert(resp.status == 400)
        resp = await client.get('/config?tenant=abc&integration_type=flight-information-system')
        assert(resp.status == 404)