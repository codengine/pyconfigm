import motor.motor_asyncio
from settings import DATABASE


class Mongo(object):
    def __init__(self):
        host = DATABASE["host"]
        port = DATABASE["port"]
        db = DATABASE["db"]
        self.client = motor.motor_asyncio.AsyncIOMotorClient(host, port)
        self.db = self.client[db]
        self.config_collection_name = "config"

    def get_client(self):
        return self.client

    def get_db(self):
        return self.db

    async def get_config(self, tenant, integration_type):
        config_collection_instance = self.db[self.config_collection_name]
        config_collection = await config_collection_instance.find_one({ 'tenant': tenant, 'integration_type': integration_type }, { '_id': 0 })
        return config_collection

    async def update_config(self, tenant, integration_type, configuration):
        try:
            config_collection_instance = self.db[self.config_collection_name]
            # await config_collection_instance.delete_many({ 'tenant': tenant, 'integration_type': integration_type})
            config_collection = await config_collection_instance.find_one({ 'tenant': tenant, 'integration_type': integration_type})
            if not config_collection:
                config_object = {
                    "tenant": tenant,
                    "integration_type": integration_type,
                    "configuration": configuration
                }
                result = await config_collection_instance.insert_one(config_object)
                config_collection = await config_collection_instance.find_one({ 'tenant': tenant, 'integration_type': integration_type}, { '_id': 0 })
                return config_collection, True
            else:
                existing_configuration = config_collection['configuration']
                new_configuration = configuration
                merged_configuration = {**existing_configuration, **new_configuration}
                config_object = {
                    "tenant": tenant,
                    "integration_type": integration_type,
                    "configuration": merged_configuration
                }
                result = await config_collection_instance.replace_one({ 'tenant': tenant, 'integration_type': integration_type}, config_object)
                config_collection = await config_collection_instance.find_one({ 'tenant': tenant, 'integration_type': integration_type}, { '_id': 0 })
                return config_collection, False
        except Exception as exp:
            print(str(exp))