from molotov import scenario


API_END_POINT = 'http://127.0.0.1:8084'


@scenario(weight=30)
async def scenario_one(session):
    async with session.get(API_END_POINT+"/") as resp:
        assert resp.status == 200, resp.status
        json_resp = await resp.json()
        assert(resp.status == 200)
        assert(json_resp == {"status": "OK"})


@scenario(weight=30)
async def scenario_two(session):
    async with session.get(API_END_POINT+"/config?tenant=acme&integration_type=flight-information-system") as resp:
        json_resp = await resp.json()
        assert(resp.status in [200, 404])


@scenario(weight=30)
async def scenario_three(session):
    post_body = {
        "tenant": "acme",
        "integration_type": "flight-information-system",
        "configuration":
            {
                "username": "acme_user",
                "password": "acme12345",
                "wsdl_urls":
                    {
                        "session_url": "https://session.manager.svc",
                        "booking_url": "https://booking.manager.svc",
                        "new_url": "asd1",
                        "new_url_2": "Hello"
                    }
            }
    }
    async with session.post(API_END_POINT + "/config", json=post_body) as resp:
        json_resp = await resp.json()
        assert (resp.status in [200, 201, 500])


# molotov load_tests/load_test.py -p 20 -w 400 -d 60 -qx
# molotov --max-runs 10000 -x load_tests/load_test.py
