from validators.validator import AbstractValidator


class GETRequestValidator(AbstractValidator):
    @classmethod
    def validate(cls, json_content):
        if "tenant" not in json_content:
            return False
        if "integration_type" not in json_content:
            return False
        return True