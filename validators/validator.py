from abc import ABC, abstractmethod


class AbstractValidator:
    @classmethod
    def validate(cls, json_content):
        raise NotImplementedError("vaidate() not implemented.")