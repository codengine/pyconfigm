from validators.validator import AbstractValidator


class RequestValidator(AbstractValidator):
    @classmethod
    def validate(cls, json_content):
        if "tenant" not in json_content:
            return False
        if "integration_type" not in json_content:
            return False
        if "configuration" not in json_content:
            return False
        configuration = json_content["configuration"]
        if not configuration:
            return False
        return True