from functools import wraps
from config.http_methods import HttpMethod
from handlers.http_bad_request import HttpBadRequest
from loaders.loaders import load_module
from settings import VALIDATORS


def enable_validators(validators=VALIDATORS):
    def enable_validators_wrapper(func):
        @wraps(func)
        async def activate_validators(request, DB):
            method = request.method
            if method == HttpMethod.GET.value:
                query_params = request.query
                json_content = {key: value for key, value in query_params.items()}
            elif method in HttpMethod.get_supported_methods():
                json_content = await request.json()
            load_validator = lambda m: load_module(m)
            validator_list = [load_module(m) for m in validators]
            # validators = list(filter(lambda v: issubclass(v, AbstractValidator), validators))
            all_validated = []
            for validator in validator_list:
                if validator:
                    all_validated += [validator.validate(json_content=json_content)]
            if not all_validated:
                response = await func(request, DB)
                return response
            else:
                if all(all_validated):
                    response = await func(request, DB)
                    return response
                else:
                    return HttpBadRequest(content="Invalid request")
        return activate_validators
    return enable_validators_wrapper