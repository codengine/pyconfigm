from functools import wraps
from config.http_methods import HttpMethod
from decorators.db_wrapper import wrap_db
from urls import URLS


def enable_routers(db):
    def enable_routers_wrapper(func):
        def add_router_handler(app, url_prefix, method, handle):
            wrapped_handled = wrap_db(handle, DB=db)
            handle_params = (url_prefix, wrapped_handled)
            if method == HttpMethod.GET:
                app.router.add_get(*handle_params)
            elif method == HttpMethod.POST:
                app.router.add_post(*handle_params)
            elif method == HttpMethod.PUT:
                app.router.add_put(*handle_params)

        @wraps(func)
        def embed_urls(*args, **kwargs):
            if args:
                instance = args[0]
                app_instance = getattr(instance, 'app')
                for url_info in URLS:
                    add_router_handler(app_instance, url_info[0], url_info[1], url_info[2])
            func_response = func(*args, **kwargs)
            return func_response
        return embed_urls
    return enable_routers_wrapper




