def wrap_db(func, DB):
    async def inner_wrapper(request):
        response = await func(request, DB)
        return response
    return inner_wrapper
