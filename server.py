from aiohttp import web
from database.mongo import Mongo
from decorators.enable_routers import enable_routers

DB = Mongo()


class AsyncServer(object):
    def __init__(self):
        self.app = web.Application()
        
    @enable_routers(db=DB)
    def start(self, host=None, port=None):
        try:
            host = host or '127.0.0.1'
            port = port or 8080
            web.run_app(self.app, host=host, port=port)
        except Exception as exp:
            print(str(exp))


if __name__ == "__main__":
    server = AsyncServer()
    server.start(host='0.0.0.0', port=8084)