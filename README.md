Twyla Project
=================================================================

Programming Language and Libraries Used: 
1. Python3.6.4
2. aiohttp==3.0.9
3. coverage==4.5.1
4. locust==0.8
5. molotov==1.4
6. motor==1.2.1
7. pytest==3.4.2
8. pytest-cov==2.5.1
9. Mongo DB latest version

requirements.txt contains all the modules with version info.


Details:
There are two endpoints /, /config with three methods support.

/ allows get requests and returns success status with 200 status.
/config POST allows config create if not exists and update with merge if exists.

The API returns appropriate status codes as below
1. 200 - Successful request with GET, POST(Update)
2. 201 - Created with POST Create
3. 500 - Internal Server Error
4. 400 - Bad Request

I have tried to write generic code so that extendibility becomes much easier.

Say I want to create a new endpoint then I just need to write a new view and add that end point in urls.py
with three arguments.

(url prefix, http method, handler view) and that's it.


In case of custom validation of requests data one just need to write a validator which extends from
AbstractValidator an implement the method validate. To use this validator wrap this in the target
view like below

@enable_validators(validators=['validators.get_request_validator.GETRequestValidator'])

So the framework is highly etendible.


Deliverables:
1. Project Source: https://bitbucket.org/codengine/pyconfigm
2. Twyla Project Demo - A Demo video showing the usage of the api through postman client and running
unit tests, integration tests, test coverage and also load tests.
3. Unit Test: tests/* contains all unit tests.
4. Integration Tests: tests_integration/* contain all integration testing
5. Load Tests: load_tests/* contain all load testing
6. Coverage Report: htmlcov contains coverage report in html and .coverage contains coverage report in txt.

For unit testing I have used pytest module
For Integration Testing I have used pytest plugin in aiohttp
For Load Testing I have used molotov based on aiohttp.
For coverage I have used pytest-cov module

How To Run:

1. Install python3.6+
2. Install MongoDB
3. Create a virtualenv
4. Activate the virtualenv
5. Install everything from requirements.txt
6. Run server.py
7. To change port just open the server.py and enter the new port number there.

How To Run Tests:

Unit Tests and Integration Tests: py.test

Load Tests: cd to the project dir and run molotov --max-runs 10000 -x load_tests/load_test.py.
Lots of parameters out there

Coverage Report:
For text report simply: py.test --cov

For html Report: py.test --cov --cov-report=html